﻿import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GithubService {
    private username: string;
    //j6bWSzH14uwj_bMK2UeE Gitlab access token
    private client_id = '26e64893c51ed1fa9766';
    private client_secret = '807e94094dd194c4757bdbdd3e67faf03815a546';

    constructor(private _http: Http) {
        console.log('Github Service Ready...');
        this.username = 'philknowles';
    }

    getUser() {
        return this._http.get('http://api.github.com/users/' + this.username + '?client_id=' + this.client_id +'&client_secret=' + this.client_secret)
            .map(res => res.json());
    }
    getRepos() {
        return this._http.get('http://api.github.com/users/' + this.username + '/repos?client_id=' + this.client_id + '&client_secret=' + this.client_secret)
            .map(res => res.json());
    }

    updateUser(username: string) {
        this.username = username;
    }
}